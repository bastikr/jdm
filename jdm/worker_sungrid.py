import os
import subprocess
import pika
import worker
import jdm


jobsdir = ".jdm-jobs/"
jobslogdir = jobsdir + "logs/"

QSUB_CMD = (
    "qsub",
        "-q", "all.q",
        "-terse",
        "-o", jobslogdir+"stdout.default",
        "-e", jobslogdir+"stderr.default",
#        "-o", jobslogdir+"stdout.$JOB_ID",
#        "-e", jobslogdir+"stderr.$JOB_ID",
        "-cwd"
)

FILE_DIR = os.path.dirname(os.path.realpath(__file__))
WRAPPER_PATH = os.path.join(FILE_DIR, "jdm_job_wrapper.py")


class Worker(worker.WorkerInterface):
    def __init__(self, **kwargs):
        worker.WorkerInterface.__init__(self, **kwargs)
        self.active_jobs = {}

        # Make sure the necessary directories exist.
        if not os.path.exists(jobsdir):
            os.makedirs(jobsdir)
        if not os.path.exists(jobslogdir):
            os.makedirs(jobslogdir)
        for name in os.listdir(jobsdir):
            if name.startswith("job."):
                path = os.path.join(jobsdir, name)
                if os.path.isfile(path):
                    os.remove(path)
        for name in os.listdir(jobslogdir):
            if name.startswith("stdout.") or name.startswith("stderr."):
                path = os.path.join(jobslogdir, name)
                if os.path.isfile(path):
                    os.remove(path)

    def do(self, channel, jobname, scriptname, parameters, jobid, ack_tag):
        print("Job {jobid} in preparation.".format(jobid=str(jobid)))
        name = subprocess.check_output("type -p " + scriptname, shell=True)
        scriptname = name.strip("\n")
        job_parameters = list(p.replace(" ", r"^") for p in parameters)
        qsub = list(QSUB_CMD) + ["-N", jobname]
        params = qsub + [WRAPPER_PATH, scriptname] + job_parameters
        sge_id = subprocess.check_output(params).strip("\n")
        jobsfile = jobsdir+"job." + str(sge_id)
        with open(jobsfile, "a") as f:
            pass
        try:
            self.active_jobs[jobid] = (sge_id, ack_tag)
        except BaseException, e:
            print(e)
            raise
        self.send_ack(channel, ack_tag)
        print("Job {jobid} started with ID {sge_id}.".format(jobid=str(jobid), sge_id=sge_id))

    def monitor_sungrid(self):
        finnished_jobs = []
        for jobid in self.active_jobs.keys():
            sge_id, ack_tag = self.active_jobs[jobid]
            result = subprocess.call("qstat -j "+sge_id, stdout=open(os.devnull, 'wb'), stderr=open(os.devnull, 'wb'), shell=True)
            if result==1: # Job does not exist
                finnished_jobs.append((jobid, sge_id, ack_tag))
        return finnished_jobs

    def finnish_job(self, channel, jobid, sge_id, ack_tag):
        #self.send_ack(channel, ack_tag)
        del self.active_jobs[jobid]
        jobsfile = jobsdir+"/job."+sge_id
        log_str = "Sungrid finished job {jobid} with ID {sge_id}: "
        log_str = log_str.format(jobid=jobid, sge_id=sge_id)
        if not os.path.exists(jobsfile):
            print(log_str+" No job.ID file found!")
            return
        with open(jobsfile, "r") as f:
            for line in f:
                if line.startswith("Exit status: "):
                    print(log_str+line)
                    break
        os.remove(jobsfile)

    def start(self):
        self.connection = pika.BlockingConnection(self.connection_params)
        self.channel = self.connection.channel()
        self.channel.queue_declare(self.queue)
        self.channel.basic_qos(prefetch_count=self.parallel_jobs)
        while True:
            self.loop()

    def loop(self):
        queue_empty = False
        while len(self.active_jobs)<self.parallel_jobs:
            method, header, body = self.channel.basic_get(queue=self.queue)
            if method is None:
                queue_empty = True
                break
            self._callback(self.channel, method, header, body)
        finnished_jobs = self.monitor_sungrid()
        for jobid, sge_id, ack_tag in finnished_jobs:
            self.finnish_job(self.channel, jobid, sge_id, ack_tag)
        if queue_empty:
            self.connection.sleep(5)


jdm.WorkerClasses["sungrid"] = Worker

if __name__=="__main__":
    worker = Worker()
    worker.start()
