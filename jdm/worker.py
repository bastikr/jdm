import os
import pika
import message
import config
import jdm_logging


class WorkerInterface:
    def __init__(self, **kwargs):
        self.logfile = kwargs.get("worker_logfile")
        self.activate_logging()
        conf = config.get_config(**kwargs)
        self.conf = conf
        self.connection_params = conf["connection_params"]
        self.working_directory = conf["working_directory"]
        self.queue = conf["queue"]
        self.parallel_jobs = conf["parallel_jobs"]
        self.jobid = 0
        if self.working_directory:
            os.chdir(self.working_directory)
            print("Working directory changed to: '%s'" % self.working_directory)
        else:
            print("Use current working directory: '%s'" % os.getcwd())
        print("Queue: "+str(self.queue))
        print("Parallel Jobs: "+str(self.parallel_jobs))

    def do(self, channel, jobname, scriptname, parameters, jobid, ack_tag):
        raise NotImplementedError()

    def activate_logging(self):
        if self.logfile:
            jdm_logging.activate_logging(self.logfile)
            jdm_logging.logstdout()
            jdm_logging.logstderr()

    def send_ack(self, channel, ack_tag):
        channel.basic_ack(delivery_tag=ack_tag)

    def _callback(self, channel, method, properties, body):
        jobname, scriptname, parameters = message.xml2job(body)
        self.jobid += 1
        ack_tag = method.delivery_tag
        self.do(channel, jobname, scriptname, parameters, self.jobid, ack_tag)

    def start(self):
        connection = pika.BlockingConnection(self.connection_params)
        channel = connection.channel()
        channel.queue_declare(self.queue)
        channel.basic_qos(prefetch_count=self.parallel_jobs)
        channel.basic_consume(self._callback, queue=self.queue)
        channel.start_consuming()

