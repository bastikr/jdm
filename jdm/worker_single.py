import subprocess
import worker
import jdm

class Worker(worker.WorkerInterface):
    def do(self, channel, jobname, scriptname, parameters, jobid, ack_tag):
        print("="*79)
        print("Job %s started:" %str(jobid))
        print("[o] Find script '%s'" % scriptname)
        name = subprocess.check_output("type -p " + scriptname, shell=True)
        scriptname = name.strip("\n")
        print("[o] Use script '%s'" % scriptname)
        print("[o] with parameters: " + str(parameters))
        params = [name.strip("\n")]
        params.extend(parameters)
        proc = subprocess.check_call(params)
        if proc==0:
            self.send_ack(channel, ack_tag)
        print("[x] Job %s done." % str(jobid))

jdm.WorkerClasses["single"] = Worker

if __name__=="__main__":
    worker = Worker()
    worker.start()
