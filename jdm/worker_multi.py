import worker
import subprocess
#from multiprocessing.pool import ThreadPool as Pool
from multiprocessing import Pool
import jdm
import jdm_logging

jobstr = [
    "="*60,
    "Job {jobid} started:",
    "[o] run '{scriptname}'",
    "[o] with parameters: {parameters}",
    "="*60,
]
jobstr = "\n".join(jobstr)

def do_async(scriptname, parameters, jobid):
    d = {}
    d["scriptname"] = scriptname
    d["parameters"] = str(parameters)
    d["jobid"] = str(jobid)
    print(jobstr.format(**d))
    params = [scriptname]
    params.extend(parameters)
    proc = subprocess.Popen(params, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    for line in proc.stdout:
        print(line)
    proc.wait()
    print("[x] Job {jobid} done.".format(**d))

def worker_log(logfile):
    if logfile:
        jdm_logging.activate_logging(logfile)
        jdm_logging.logstdout()
        jdm_logging.logstderr()
    print("Worker for pool created.")


class Worker(worker.WorkerInterface):
    def __init__(self, *args, **kwargs):
        worker.WorkerInterface.__init__(self, *args, **kwargs)
        self.pool = Pool(self.parallel_jobs, initializer=worker_log, initargs=(self.logfile,))

    def do(self, channel, jobname, scriptname, parameters, jobid, ack_tag):
        print("Add job %s to pool." % str(jobid))
        args = [scriptname, parameters, jobid]
        ack = lambda x:self.send_ack(channel, ack_tag)
        self.pool.apply_async(do_async, args, callback=ack)

jdm.WorkerClasses["multi"] = Worker

if __name__=="__main__":
    worker = Worker()
    worker.start()
