#!/bin/env python2

import os
import sys
import subprocess

jobsdir = ".jdm-jobs"
assert os.path.exists(jobsdir)

job_id = os.environ["JOB_ID"]
jobsfile = jobsdir+"/job." + str(job_id)

d = {}
d.update(**os.environ)
d["args"] = str(sys.argv)

info_str = """\
name: {JOB_NAME}
hostname: {HOME}
queue: {QUEUE}
arguments: {args}
""".format(**d)

with open(jobsfile, "w") as f:
    f.write(info_str)

cmd = list(p.replace("^", " ") for p in sys.argv[1:])
exit_status = subprocess.call(cmd)

with open(jobsfile, "a") as f:
    f.write("Exit status: "+str(exit_status)+"\n")
