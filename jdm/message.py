import xml.etree.ElementTree as xml

def job2xml(scriptname, parameters, jobname=None):
    job_dom = xml.Element("job")
    if jobname is not None:
        job_dom.attrib["jobname"] = jobname
    scriptname_dom = xml.SubElement(job_dom, "scriptname")
    scriptname_dom.text = scriptname
    parameters_dom = xml.SubElement(job_dom, "parameters")
    for p in parameters:
        p_dom = xml.SubElement(parameters_dom, "parameter")
        p_dom.text = str(p)
    return xml.tostring(job_dom)

def xml2job(xml_string):
    job_dom = xml.fromstring(xml_string)
    scriptname_dom = job_dom[0]
    scriptname = scriptname_dom.text
    jobname = job_dom.attrib.get("jobname")
    parameters_dom = job_dom[1]
    parameters = []
    for p_dom in parameters_dom:
        parameters.append(p_dom.text)
    return jobname, scriptname, parameters

