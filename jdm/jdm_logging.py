import logging
import sys

class LoggerFileInterface:
    """
    Fake file-like stream object that redirects writes to a logger instance.
    """
    def __init__(self, logger, log_level):
        self.logger = logger
        self.log_level = log_level
        self.linebuf = ''

    def write(self, buf):
        for line in buf.rstrip().splitlines():
            self.logger.log(self.log_level, line.rstrip())

    def flush(self):
        pass

def activate_logging(path):
    logging.basicConfig(filename=path, level=logging.INFO)

def logstdout():
    stdout_logger = logging.getLogger('STDOUT')
    sys.stdout = LoggerFileInterface(stdout_logger, logging.INFO)

def logstderr():
    stderr_logger = logging.getLogger('STDERR')
    sys.stderr = LoggerFileInterface(stderr_logger, logging.ERROR)

