import os
PIDFILE = os.path.expanduser("~/.jdm.pid")
CONFIGFILEPATH = os.path.expanduser("~/.jdm.config")

import config
import worker

WorkerClasses = {}

import worker_single
import worker_multi
import worker_sungrid
