import pika
import message
import config


def get_target(**kwargs):
    conf = config.get_config(**kwargs)
    connection_params = conf["connection_params"]
    queue = conf["queue"]
    connection = pika.BlockingConnection(connection_params)
    channel = connection.channel()
    channel.queue_declare(queue)
    target = {"connection": connection, "channel": channel, "queue": queue}
    return target

def send_job(target, scriptname, parameters, jobname=None):
    channel = target["channel"]
    queue = target["queue"]
    m = message.job2xml(scriptname, parameters, jobname=jobname)
    channel.basic_publish(exchange="", routing_key=queue, body=m)

def close_target(target):
    target["connection"].close()

if __name__=="__main__":
    target = get_target()
    send_job(target, "ls", ["-a"])
    close_target(target)
