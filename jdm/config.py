import os
import ConfigParser
import pika
import jdm

parser = ConfigParser.SafeConfigParser(allow_no_value=True)


def read_configfile(path):
    path = os.path.expanduser(path)
    assert os.path.exists(path)
    parser.read(path)

    conf = {}

    # Authentication
    conf["username"] = parser.get("authentication", "username")
    conf["password"] = parser.get("authentication", "password")

    # Server:
    conf["host"] = parser.get("server", "host")
    port = parser.get("server", "port")
    if port:
        conf["port"] = int(port)
    conf["queue"] = parser.get("server", "queue")

    # Worker
    conf["parallel_jobs"] = parser.getint("worker", "parallel_jobs")
    conf["working_directory"] = parser.get("worker", "working_directory")
    conf["default_worker"] = parser.get("worker", "default_worker")
    name = parser.get("worker", "pidfile")
    if name:
        conf["worker_pidfile"] = os.path.expanduser(name)
    name = parser.get("worker", "logfile")
    if name:
        conf["worker_logfile"]  = os.path.expanduser(name)
    return conf


def get_config(configfile=True, **kwargs):
    if isinstance(configfile, str):
        conf = read_configfile(configfile)
        conf.update(kwargs)
    elif read_configfile:
        conf = read_configfile(jdm.CONFIGFILEPATH)
        conf.update(kwargs)
    else:
        conf = kwargs.copy()

    server = {}
    username = conf.get("username")
    password = conf.get("password")
    if username:
        server["credentials"] = pika.PlainCredentials(username, password)
    server["host"] = conf.get("host")
    if conf.get("port"):
        server["port"] = int(port)
    conf["connection_params"] = pika.ConnectionParameters(**server)
    wd = conf.get("working_directory")
    if wd:
        conf["working_directory"] = os.path.expanduser(wd)
    return conf
